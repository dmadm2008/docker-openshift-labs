# Docker Lab

## Briefing
We need to create a multi service application consisting of three services:

- router
- frontend
- backend

The Router depending on a given URL should forward requests to backend or frontend.
The Frontend should return static web pages.
The Backend implements some logic.

The both Router and Frontend are run on Nginx.
The Router is able to handle such URLs:

- /static/... - forwarding requests to the Frontend
- /api/... - forwarding requesting to the Backend


## You should complete the following

### Build a common image for the Router and the Frontend

- The image should have only installed Nginx package and be able to run with default settings.
- Nginx should be accessible by a container port 8080. HTTPS is not required.
- it should
- The image should be build from CentOS7 image - you can find it on hub.docker.com.
- Its Dockerfile should have the CMD instruction as this
    ```CMD [ "nginx", "-c", "/etc/nginx/nginx.conf" ]```

### Run a Router instance from the image above

- Config default.config should be given to the instance as __/etc/nginx/nginx.conf__. The sections 'location /static {}' and 'location /api {}' are most important. Feel free to modify
    the rest parts as you need although the given config should already work.


```
worker_processes                            auto;
error_log                                   /dev/stdout;
pid                                         /tmp/nginx.pid;
http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
    access_log                              /dev/stdout  main;
    include                                 /etc/nginx/mime.types;
    default_type                            application/octet-stream;

    server {
        listen                              8080 default_server;
        server_name                         _;
        root                                /usr/share/nginx/html;
        include                             /etc/nginx/default.d/*.conf;
        location /static {
            proxy_pass                      frontend:8080;
        }
        location /api {
            proxy_pass                      backend:8080;
        }
    }
}
```

### Build The Frontend image and run a docker instance from it
    - You should reuse the Router image.
    - Nginx's configuration might look like this:
    
```
worker_processes                            auto;
error_log                                   /dev/stdout;
pid                                         /tmp/nginx.pid;
http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
    access_log                              /dev/stdout  main;
    include                                 /etc/nginx/mime.types;
    default_type                            application/octet-stream;
    include                                 /etc/nginx/conf.d/*.conf;

    server {
        listen                              8080 default_server;
        server_name                         _;
        root                                /usr/share/nginx/html;
        include                             /etc/nginx/default.d/*.conf;
        location /static {
            root                            /web-pages
        }
    }
}
```

- based on the above config create two web pages in '/web-pages':

      /web-pages/hello.html:
        <p>Hello, FrontEnd!</p>
      /web-pages/ping:
        pong
        
- The Trontend should not be accessible for external connections (don't use parameter "-p" in docker run). Instead, use '--link' when you run the Router instance.

### Build The Backend and run a docker instance from it:

    - The backend is a simple Flask application. Build an image include that Flask app
    - Create your Flask app like this (from here https://pythonspot.com/flask-web-app-with-python/):

```
$ pip install Flask
Create a file called hello.py

from flask import Flask
app = Flask(__name__)

@app.route("/api/hello")
def hello():
    return "Hello Backend!"

if __name__ == "__main__":
    app.run()
```

Finally run the web app using this command:

```
$ python hello.py
* Running on http://localhost:5000/
```
    - the backend should not expose any ports externally (use --link option for docker run)

## Summary

    - Need to build 3 Images:
        1. for router
        2. for backend
        3. for frontend
    - the frontend image should be build from the router's image
    - the router should not have custom configs in it. A router docker container should mount the custom config upon starting
    - the frontend should have the custom config and needed html files built in its image
    - the backend should also have installed the Flask framework

At the end your solution should have the following files:

    - Dockerfile.nginx
    - Dockerfile.frontend
    - Dockerfile.backend
    - frontend.files/hello.html
    - frontend.files/ping
    - backend.files/hello.py
    - build_app.sh - it builds the app images
    - start_app.sh - it runs the app in the right order
    - stop_app.sh - it stops and destroys the containers

## Checking
We will check it by this way:

- curl http://openshift-X.eastus2.cloudapp.azure.com/static/hello -> Hello, Frontend!
- curl http://openshift-X.eastus2.cloudapp.azure.com/static/ping  -> pong
- curl http://openshift-X.eastus2.cloudapp.azure.com/api/hello    -> Hello, Backend!

