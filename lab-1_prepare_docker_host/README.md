# Lab 1. Configuring VM

There is a list of virtual machines provisioned for every participant.

- openshift-10.*
- openshift-11.*
- openshift-12.*
- openshift-13.*
- openshift-14.*
- openshift-15.*
- openshift-16.*
- openshift-17.*
- openshift-18.*
- openshift-19.*
- openshift-20.*

These VMs have:

- installed OS Red Hat 7.7
- 1 vCPU, 1.75 Gib RAM, 30 GB of disk space
- account __openshiftX__ with password _CSopenshift123_ (X - is an ordered number of VM)
- account __csadmin__ with password _CSadmin_p123_

You can access to the VMs using a ssh client:

- if you are on Windows - download Putty from https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html (portable version)
- if you are on Mac or Linux - you should have something in there

Both accounts __openshiftX__ and __csadmin__ enabled to use sudo without restrictions. We will use user __openshiftX__ unless said other.


# Lab 1: Installing and configuring the Docker environment

## Installing Packages

Pay attention on which commands require raising permissions with __sudo__.

_Check if the docker package is installed or not:_


    yum info docker


Check the output

```
Name        : docker
Arch        : x86_64
Epoch       : 2
Version     : 1.13.1
Release     : 104.git4ef4b30.el7
Size        : 65 M
Repo        : installed
From repo   : rhui-rhel-7-server-rhui-extras-rpms
Summary     : Automates deployment of containerized applications
URL         : https://github.com/docker/docker
License     : ASL 2.0
Description : Docker is an open-source engine that automates the deployment of any
            : application as a lightweight, portable, self-sufficient container that will
            : run virtually anywhere.
            :
            : Docker containers can encapsulate any payload, and will run consistently on
            : and between virtually any server. The same container that a developer builds
            : and tests on a laptop will run at scale, in production*, on VMs, bare-metal
            : servers, OpenStack clusters, public instances, or combinations of the above.
```

if the line __Repo : installed__ is there that means that the package is installed.

_If it is not, we need to do that._

### Installing Docker service

_Install the docker package:_


    sudo yum install docker     # and answer 'Y' when prompted


_Start the Docker service (daemon):_


    sudo systemctl start docker


_Check that the service is running:_


    systemctl is-active docker


or, more detailed output


    systemctl status docker


_Check that the service will be started automatically after rebooting:_


    systemctl is-enabled docker


_if it is not, enable it:_


    sudo systemctl enable docker


## Check the Docker Command


By this time the docker service is ready to run containers. Try it out:


    sudo docker ps


you should get this output:

```
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```

But we don't want use sudo every time we execute the docker command. Check it:

```
$ docker ps

Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get http://%2Fvar%2Frun%2Fdocker.sock/v1.26/containers/json: dial unix /var/run/docker.sock: connect: permission denied
```

## Giving Normal Users Permissions to Access Docker Service

__Check who is allowed to read from and write to the mentioned socket:__

```
$ ls -l /var/run/docker.sock
srw-rw----. 1 root root 0 Oct 22 11:07 /var/run/docker.sock
```



It shows the root user and members of the __root__ group can access to it. Our user is not there. Let's fix it.
The most simple method to do that is to create some special group and tell the Docker service to allow users of that group 
to use the socket.

### Configuring Docker daemon

In here we will configure the Docker service to allow a specific group to access the socket `/var/run/docker.sock`.

#### Creating a unix group

    sudo groupadd dockerroot
    
#### Configuring Docker daemon

Modify the file `/etc/docker/daemon.json`. If the file does not exist, create it:

    $ sudo vi /etc/docker/daemon.json
    {
        "group": "dockerroot"
    }
    
__Restart the Docker daemon:__

    $ sudo systemctl restart docker
    
__Check the socket file permissions now:__

    $ ls -l /var/run/docker.sock
    srw-rw----. 1 root dockerroot 0 Oct 22 11:07 /var/run/docker.sock

As of now, users in group __dockerroot__ have access to the docker service.


### Adding a User into a Group

_(here I use VM openshift-10, my user has the name __openshift10__ accordingly. Use your VM)_

__Check in what groups we have been added:__

    $ id
    uid=1001(openshift10) gid=1001(openshift10) groups=1001(openshift10),1000(csadmin) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023

__Add ourselves to the _dockerroot_ group:__

    sudo usermod -a -G dockerroot openshift10

__Check it again__

    $ id
    uid=1001(openshift10) gid=1001(openshift10) groups=1001(openshift10),993(dockerroot),1000(csadmin) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023

So now, we are a member of group __dockerroot__. In order to the change take in effect, close the ssh session and log in to the VM again.

__Try to use the docker command now:__

    $ docker ps
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES

This output tells that the docker command can talk to the docker daemon - we got the needed permissions for that.

## Run a Hello World container

Execute this command:

```
$ docker run hello-world
Unable to find image 'hello-world:latest' locally
Trying to pull repository registry.access.redhat.com/hello-world ...
Pulling repository registry.access.redhat.com/hello-world
Trying to pull repository docker.io/library/hello-world ...
latest: Pulling from docker.io/library/hello-world
1b930d010525: Pull complete
Digest: sha256:c3b4ada4687bbaa170745b3e4dd8ac3f194ca95b2d0518b417fb47e5879d9b5f
Status: Downloaded newer image for docker.io/hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

__Check what docker has created for us:__

```
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
[openshift10@openshift-10 ~]$ docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                      PORTS               NAMES
f443c09c499f        hello-world         "/hello"            43 seconds ago      Exited (0) 41 seconds ago                       youthful_euclid
[openshift10@openshift-10 ~]$ docker images
REPOSITORY              TAG                 IMAGE ID            CREATED             SIZE
docker.io/hello-world   latest              fce289e99eb9        9 months ago        1.84 kB
```

So the container was run, displayed the message as demonstrated above, and completed. Now the container is stopped:

- we don't see it in `docker ps` output
- we can find it in `docker ps -a` output
- we can see the image which was used to run the container `docker images`

## Material to Learn

1. Installing packages with `yum` command: man yum, yum install --help
2. Running and stopping services: `systemctl start/stop/enable/disable <service>`
3. Configuring the Docker service to enable normal users to access
4. Run containers with docker command: `man docker run`, `docker run --help`

That's all!